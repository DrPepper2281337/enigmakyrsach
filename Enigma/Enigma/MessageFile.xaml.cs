﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Enigma
{
    /// <summary>
    /// Логика взаимодействия для MessageFile.xaml
    /// </summary>
    public partial class MessageFile : Window
    {
        public MessageFile()
        {
            InitializeComponent();
        }
        MainWindow mainWindow = new MainWindow();
        private void BntEncrypted_Click(object sender, RoutedEventArgs e)
        {
            if (!(MainWindow.EncryptedMessage == "" || MainWindow.EncryptedMessage == null))
            {
                mainWindow.SaveFile(MainWindow.EncryptedMessage, mainWindow.PathFileGet());
            }
            else
            {
                MessageBox.Show("Поле \"Tекст\" пустое!");
            }
            this.Close();
        }

        private void BtnDecrypted_Click(object sender, RoutedEventArgs e)
        {
            if (!(MainWindow.DecryptedMessage == "" || MainWindow.DecryptedMessage == null))
            {
                mainWindow.SaveFile(MainWindow.DecryptedMessage, mainWindow.PathFileGet());
            }
            else
            {
                MessageBox.Show("Поле \"Преобразованный текст\" пустое!");
            }
            this.Close();
        }
    }
}
