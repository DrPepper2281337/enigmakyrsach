﻿using System;
using System.Diagnostics;
using Enigma;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EnigmaTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Encrypted_message_привет_мир_key_скорпион_return_бычтфы_ыцв()
        {
            //arrange
            char[] message = "привет мир".ToCharArray();
            char[] key = "скорпион".ToCharArray(); 
            char[] expected = "бычтфы ыцв".ToCharArray();

            //act
            MainWindow mainWindow = new MainWindow();
            char[] actual = mainWindow.Encrypted(message, key);

            //assert
            Assert.AreEqual(expected.ToString(), actual.ToString());
        }

        [TestMethod]
        public void Decrypted_message_бычтфы_ыцв_key_скорпион_return_привет_мир()
        {
            //arrange
            char[] message = "бычтфы ыцв".ToCharArray();
            char[] key = "скорпион".ToCharArray();
            char[] expected = "привет мир".ToCharArray();

            //act
            MainWindow mainWindow = new MainWindow();
            char[] actual = mainWindow.Decrypted(message,key);

            //assert
            Assert.AreEqual(expected.ToString(), actual.ToString());
        }

        [TestMethod]
        public void SaveFileTests()
        {
            //arrange
            bool expected = true;

            //act
            MainWindow mainWindow = new MainWindow();
            string path = mainWindow.PathFileGet();
            bool actual = mainWindow.SaveFile("", path);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReadAllTextFileTest()

        {
            //arrange
            string expected = "";

            //act
            MainWindow mainWindow = new MainWindow();
            string path = mainWindow.PathFileGet();
            string actual = mainWindow.ReadAllTextFile(path);

            //assert
            Assert.AreEqual(expected, actual);

            Debug.WriteLine("File was readed.");
        }
    }
}
